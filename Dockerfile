# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
LABEL maintainer="Kennedy Sanchez @Sappotech" 
EXPOSE 8181
# COPY target/spring-petclinic.jar /home/spring-petclinic.jar
COPY target/petclinic.war /tmp/petclinic.war
#COPY target/*.jar /home
#ENTRYPOINT ["java","-jar","/home/spring-petclinic.jar","--server.port=8181"]
ENTRYPOINT ["java","-jar","/tmp/petclinic.war","--server.port=8181"]
# ENTRYPOINT ["java", "-jar", "*.jar","--server.port=8181"]

####################################################################
# FROM anapsix/alpine-java 
# LABEL maintainer="Kennedy Sanchez @Sappotech" 
# COPY /target/spring-petclinic.jar /home/spring-petclinic.jar 
# # COPY target/*.jar /home
# CMD ["java","-jar","/home/spring-petclinic.jar"]
# # ENTRYPOINT java -jar *.jar
####################################################################