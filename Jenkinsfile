// Petclinic is a Spring Boot application built using Maven.

pipeline {
  environment {
    registry = "ksanchez/spring-petclinic-secure-sdlc"
    registryCredential = 'dockerhubcreds'
    dockerImage = ''
  }
  agent any
  tools {
    maven 'Apache Maven 3.8.5'
    jdk 'Java version: 11.0.13'
  } 
  stages {
    stage('Cloning Git') {
      steps {
        git 'https://gitlab.com/pokoyo.bughunter/spring-framework-petclinic-secure-sdlc.git'
      }
    }
    stage('Compile') {
       steps {
         sh 'mvn compile' //only compilation of the code
       }
    }
    stage('Test') {
      steps {
        sh '''
        mvn clean install
        ''' 
        //if the code is compiled, we test and package it in its distributable format; run IT and store in local repository
      }
    }
    stage('Building Image') {
      steps{
        script {
          dockerImage = docker.build registry + ":latest"
        }
      }
    }
      stage('Trivy Vulnerability Scanner') {
          steps {
              sh """
                  rm -rf ./trivy
                  mkdir -p ./trivy
                  curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b ./trivy 
                  wget -P ./trivy https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/html.tpl
                  cd trivy
                  ./trivy image -f json -o Trivy-CVE-Report.json ksanchez/spring-petclinic-secure-sdlc      
                 """
            }
        }
    stage('Deploy Image') {
      steps{
         script {
            docker.withRegistry( '', registryCredential ) {
            dockerImage.push()
          }
        }
      }
    }
    stage('Remove Unused docker image') {
      steps{
        sh "docker rmi $registry:latest"
      }
    }
  }
}
