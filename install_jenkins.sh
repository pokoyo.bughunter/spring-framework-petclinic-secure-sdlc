#!/bin/bash
#Author: Kennedy Sanchez

# Jenkins
wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
yum upgrade
yum install -y jenkins 

# JDK
# yum install -y java-11-openjdk
amazon-linux-extras install -y java-openjdk11

#java-1.8.0-openjdk-devel amazon-linux-extras    # It Works!
amazon-linux-extras enable python3.8
yum install python38
yum update –y
yum install git -y
yum install maven -y

# Docker
yum install docker -y
sudo systemctl enable docker.service

# Services
#sudo yum install -y jenkins java-openjdk11
systemctl start docker.service
systemctl daemon-reload
systemctl enable jenkins
systemctl start jenkins
systemctl status jenkins

# Permissions
sudo usermod -a -G jenkins
usermod -a -G docker
chmod 666 /var/run/docker.sock
cat /var/lib/jenkins/secrets/initialAdminPassword
