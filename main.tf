#############################################################################
# VARIABLES
#############################################################################

variable "region_1" {
  type            = string
  default         = "us-east-1"
}

variable "region_2" {
  type           = string
  default        = "us-west-1"    # us-west-1
}

variable "vpc_name_east" {
  type           = string
  default        = "petclinic-prod-vpc-east"
  description    = "VPC East Zone"
}

variable "vpc_name_west" {
  type           = string
  default        = "petclinic-prod-vpc-west"
  description    = "VPC West Zone"
}

variable "vpc_cidr_range_east" {
  type           = string
  default        = "10.10.0.0/16"
}

variable "public_subnets_east" {
  type           = list(string)
  default        = ["10.10.0.0/24", "10.10.1.0/24"]
}

variable "private_subnets_east" {
  type          = list(string)
  default       = ["10.10.8.0/24", "10.10.9.0/24"]
}

variable "vpc_cidr_range_west" {
  type          = string
  default       = "10.11.0.0/16"
}

variable "public_subnets_west" {
  type         = list(string)
  default      = ["10.11.0.0/24", "10.11.1.0/24"]
}

variable "private_subnets_west" {
  type        = list(string)
  default     = ["10.11.8.0/24", "10.11.9.0/24"]
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "Jenkins-KP"
  description = "SSH key name in your AWS account for AWS instances."
}


#############################################################################
# PROVIDERS
#############################################################################
terraform {
  required_providers {
    aws = {
      source    = "hashicorp/aws"
      version   = "4.16.0"
    }
  }
}

provider "aws" {
  region       = var.region_1
  alias        = "east"
  profile      = "east"
}

provider "aws" {
  region       = var.region_2
  alias        = "west"
  profile      = "west" 
}

#############################################################################
# DATA SOURCES
#############################################################################

data "aws_caller_identity" "infra" {
  provider = aws.east
}

data "aws_caller_identity" "security" {
  provider = aws.west
}

data "aws_availability_zones" "azs_east" {
    provider = aws.east
}

data "aws_availability_zones" "azs_west" {
    provider = aws.west
}

data "aws_ami" "amazon_linux_east" {
  provider = aws.east
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["amazon"] # Canonical
}

data "aws_ami" "amazon_linux_west" {
  provider = aws.west
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["amazon"] # Canonical
}
#############################################################################
# RESOURCES
#############################################################################  

module "vpc_east" {
  source         = "terraform-aws-modules/vpc/aws"
  version        = "2.77.0"
  name           = var.vpc_name_east
  cidr           = var.vpc_cidr_range_east
  azs            = slice(data.aws_availability_zones.azs_east.names, 0, 2)
  public_subnets = var.public_subnets_east
  database_subnets  = var.private_subnets_east
  database_subnet_group_tags = {
    subnet_type = "database_east"
  }

  providers = {
      aws = aws.east
  }

  tags = {
    Environment = "Prod"
    Region      = "East"
    Team        = "Infra"
    Autor       = "Kennedy Sanchez @Sappotech"
  }

}

module "vpc_west" {
  source          = "terraform-aws-modules/vpc/aws"
  version         = "2.77.0"
  name            = var.vpc_name_west
  cidr            = var.vpc_cidr_range_west
  azs             = slice(data.aws_availability_zones.azs_west.names, 0, 2)
  public_subnets  = var.public_subnets_west

  database_subnets  = var.private_subnets_west
  database_subnet_group_tags = {
    subnet_type = "database_west"
  }

  providers = {
      aws = aws.west
  }

  tags = {
    Environment = "Prod"
    Region      = "West"
    Team        = "Infra"
    Autor       = "Kennedy Sanchez @Sappotech"
  }

}

resource "aws_security_group" "jenkins_sg_east" {
  provider    = aws.east
  name        = "Jenkins_Access_SG_East"
  description = "Allow Traffic"
  #vpc_id      = var.vpc_name_east    **** FIX THIS ****

  ingress {
    description      = "Allow from Personal CIDR block"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Allow SSH from Personal CIDR block"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    #cidr_blocks      = [var.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name  = "Jenkins SG East Zone"
    Env   = "Prod"
    Autor = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_security_group" "jenkins_sg_west" {
  provider    = aws.west
  name        = "Jenkins_Access_SG_West"
  description = "Allow Traffic"
  #vpc_id      = var.vpc_name_west           **** FIX THIS ****

  ingress {
    description      = "Allow from Personal CIDR block"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # cidr_blocks      = [var.cidr_block]
  }

  ingress {
    description      = "Allow SSH from Personal CIDR block"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    #cidr_blocks      = [var.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name  = "Jenkins SG West Zone"
    Env   = "Prod"
    Autor = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_instance" "jenkins_east" {
  provider        = aws.east
  ami             = data.aws_ami.amazon_linux_east.id
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.jenkins_sg_east.name]
  user_data       = "${file("install_jenkins.sh")}"

  tags = {
    Name = "Jenkins EC2 Instance East"
    Environment = "Prod"
    Region      = "East"
    Team        = "Infra"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

resource "aws_instance" "jenkins_west" {
  provider        = aws.west
  ami             = data.aws_ami.amazon_linux_west.id
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.jenkins_sg_west.name]
  user_data       = "${file("install_jenkins.sh")}"

    tags = {
    Name = "Jenkins EC2 Instance East"
    Environment = "Prod"
    Region      = "West"
    Team        = "Infra"
    Autor       = "Kennedy Sanchez @Sappotech"
  }
}

#############################################################################
# OUTPUTS
#############################################################################

output "vpc_id_east" {
  value = module.vpc_east.vpc_id
}

output "vpc_id_west" {
  value = module.vpc_west.vpc_id
}
