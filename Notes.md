# Technologies

1. `Jenkins Pipeline`
2. `Docker`
3. `Kubernetes`
4. `Terraform`

# Commands

# Initialize the terraform configuration
- `terraform init`

# Plan the terraform deployment
- `terraform plan -out vpc.tfplan`

# Apply the deployment
- `terraform apply "vpc.tfplan"`