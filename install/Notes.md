# Project Configuration Steps

- While the docker image has known CVEs reported it shouldn't go through deploy process. First it Need to be fixed.

# Terraform

### COMMANDS

# Run the aws configure command to set your access and secret keys
- `aws configure`
- `aws configure --profile east`

# Rename the terraform.tfvars.example file to terraform.tfvars and change the region
# to your desired region

# Initialize the terraform configuration
terraform init

# Plan the terraform deployment
terraform plan -out vpc.tfplan

# Apply the deployment
terraform apply "vpc.tfplan"

# Do not destroy the VPC until you have completed state migration module

###################################################################################################

# Multiple provider module commands

# Rename the peering configuration
ren peering.tf.rename peering.tf

# Run terraform plan
terraform plan -var peer_role_arn="PEER_ROLE_ARN" -var destination_vpc_id="SEC_VPC_ID" -out peer.tfplan

# Run terraform apply
terraform apply "peer.tfplan"

###################################################################################################

# State migration module

# Rename the backend config file
ren backend.tf.rename backend.tf

# Update the region, bucket, and dynamodb table
terraform init -backend-config="bucket=BUCKET_NAME" -backend-config="region=REGION_NAME" -backend-config="dynamodb_table=TABLE_NAME"

---

# PATH

- `/var/lib/jenkins/workspace/spring-petclinic-secure-sdlc-trivy`

# CI-CD With Docker, Jenkins Pipelines
# Training Building a Modern CI/CD Pipeline with Jenkins

# Spring Petclinic Framework
- https://github.com/spring-petclinic/spring-framework-petclinic
- https://github.com/talitz/spring-petclinic-jenkins-pipeline/blob/master/Jenkinsfile

- https://app.pluralsight.com/course-player?clipId=57a92ff5-273a-4d4e-85c4-bae6dbdbf8c1

# Project Build a Docker Jenkins Pipeline to Implement CI/CD Workflow.

- https://lms.simplilearn.com/courses/3045/DevOps-Certification-training/assessment
- https://www.liatrio.com/blog/building-with-docker-using-jenkins-pipelines

# How To Push a Docker Image To Docker Hub Using Jenkins
- https://blog.knoldus.com/how-to-push-a-docker-image-to-docker-hub-using-jenkins/ 

1. Install the Docker Pipelines plugin on Jenkins
2. Add Credentials. In Jenkins you have to add a new credential with your Docker Hub account.
3. Create your Jenkins pipeline.

# Setup Jenkins in Docker container
- https://blog.knoldus.com/setup-jenkins-in-docker-container/   

# Sources

- https://www.cyberciti.biz/faq/how-to-install-docker-on-amazon-linux-2/
- https://www.pulumi.com/registry/packages/aws/how-to-guides/aws-ts-k8s-voting-app/
- https://github.com/hbollon/k8s-voting-app-aws
- https://v2-1.docs.kubesphere.io/docs/quick-start/devops-online/
- https://github.com/kubesphere/devops-java-sample
- https://www.jenkins.io/blog/2017/02/07/declarative-maven-project/

# Integrating Vulnerability Scanners

# Trivy
- https://foreops.com/blog/trivy-intro/
- https://github.com/aquasecurity/trivy
- https://github.com/GandhiCloudLab/devsecops-with-trivy
- https://semaphoreci.com/blog/continuous-container-vulnerability-testing-with-trivy
- https://medium.com/alterway/adding-image-security-scanning-to-a-ci-cd-pipeline-2faff072d
- https://semaphoreci.com/blog/continuous-container-vulnerability-testing-with-trivy
- https://community.ibm.com/community/user/wasdevops/blogs/jeya-gandhi-rajan-m1/2021/09/16/recipe-devsecops-using-aquasec-trivy
- https://github.com/GandhiCloudLab/devsecops-with-trivy/#1-Integrating-Trivy-in-Jenkins


- trivy image --format template --template "/root/contrib/html.tpl" -o ./nginx-flask-vulnerability-report.html tiangolo/uwsgi-nginx-flask

# STAGE TRIVY
```sh
 stage('Trivy Vulnerability Scanner') {
          steps {
              sh """
                  rm -rf ./trivy
                  mkdir -p ./trivy
                  chmod 644 ./trivy
                  curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b ./trivy 
                  wget -P ./trivy https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/html.tpl
                  cd trivy
                  pwd
                  ls -alh
                  sudo ./trivy image --format template --template '{{- $critical := 0 }}{{- $high := 0 }}{{- range . }}{{- range .Vulnerabilities }}{{- if  eq .Severity "CRITICAL" }}{{- $critical = add $critical 1 }}{{- end }}{{- if  eq .Severity "HIGH" }}{{- $high = add $high 1 }}{{- end }}{{- end }}{{- end }}Critical: {{ $critical }}, High: {{ $high }}' ksanchez/spring-petclinic-secure-sdlc
                  sudo ./trivy image -f json -o Trivy-Vulnerability-Report.json ksanchez/spring-petclinic-secure-sdlc      
                  sudo ./trivy image --format template --template "@./html.tpl" -o Trivy-Vulnerability-Report.html ksanchez/spring-petclinic-secure-sdlc
                 """
            }
        }
```



# Anchore
# Integrate Anchore Scanning into Jenkins Pipeline
- https://anchore.com/blog/integrating-anchore-scanning-into-jenkins-pipeline-via-jenkinsfile/
- https://plugins.jenkins.io/anchore-container-scanner/

