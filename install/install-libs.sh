# GIT
sudo yum install -y git 

# DOCKER
sudo yum -y update
sudo yum install -y docker
sudo chmod 666 /var/run/docker.sock
sudo systemctl enable docker.service
# sudo usermod -a -G docker
sudo systemctl start docker.service

# JENKINS
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
# Add required dependencies for the jenkins package

# JDK
sudo yum install -y java-11-openjdk
sudo amazon-linux-extras install -y java-openjdk11

# Jenkins
sudo yum install -y jenkins
# sudo usermod -a -G jenkins
sudo systemctl daemon-reload
sudo systemctl enable jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins


# MAVEN
#sudo yum install -y maven
cd /opt
sudo wget https://downloads.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz
sudo tar xzf apache-maven-3.8.5-bin.tar.gz
sudo ln -s apache-maven-3.8.5 maven
sudo cp apache-maven-3.8.5/bin/mvn /usr/bin/
export M2_HOME=/opt/maven
export PATH=${M2_HOME}/bin:${PATH}
#source ./install-libs.sh
/opt/maven/bin/mvn --version
# mvn --version
sudo rm -rf apache-maven-3.8.5-bin.tar.gz

# Trivy Installation and Conf
#sudo curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b ./trivy 
#sudo mv ./trivy/trivy /usr/bin/trivy
#trivy --version

#wget -P ./trivy https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/html.tpl  
#trivy image --format template --template @./html.tpl -o report.html node:11-alpine


# Jenkins Creds
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# SOURCE: https://www.jenkins.io/doc/book/installing/linux/#red-hat-centos